<?php
declare(strict_types=1);

require 'Board.php';
require 'BatchFileWriter.php';

if($argc != 3) {
    echo "Первым параметром нужно передать размер поля, вторым колличество монет, например: php " . __FILE__ . " 36 18";
    exit(-1);
} else {
    $boardSize = (int)$argv[1];
    $coinsCount = (int)$argv[2];
}

try {
    $report = new BatchFileWriter('report.txt');
    $board = new Board($boardSize, $coinsCount);

    $combinationsCount = $board->getCombinationsCount();

    if($combinationsCount < 10)
        $report->write("Менее 10 вариантов");
    else {
        $report->writeInteger($combinationsCount);
        $board->findCombinations(function ($newCombination) use ($report) {
            $report->write($newCombination);
        });
    }
} catch (\Exception $e) {
    echo $e->getMessage();
} finally {
    unset($report);
}
