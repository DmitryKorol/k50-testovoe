<?php

/**
 * Class BatchFileWriter
 *
 * Записывает строки в файл, выгружает на диск когда набирает определенное колличество строк
 */
class BatchFileWriter
{
    /** @var bool|resource */
    private $_fileHandle;

    /** @var integer */
    private $_batchSize;

    /** @var array */
    private $_batch = [];

    /** @var bool */
    private $_fileIsEmpty = true;

    public function __construct(string $filename, int $batchSize = 100)
    {
        $this->_fileHandle = fopen($filename, 'w');
        $this->_batchSize = $batchSize;
    }

    public function __destruct()
    {
        $this->_saveToDisk();
        fclose($this->_fileHandle);
    }

    /**
     * Добавляет строку в очередь на запись
     * @param string $data
     */
    public function write(string $data) : void
    {
        $this->_batch[] = $data;

        if(count($this->_batch) >= $this->_batchSize)
            $this->_saveToDisk();
    }

    /**
     * Добавляет в очередь на запись строку с числом
     * @param int $number
     */
    public function writeInteger(int $number) : void
    {
        $this->write((string)$number);
    }

    /**
     * Выгружает из очереди на диск
     */
    private function _saveToDisk() : void
    {
        $data = implode("\n", $this->_batch);

        if(!$this->_fileIsEmpty)
            $data = "\n" . $data;

        fwrite($this->_fileHandle, $data);

        $this->_batch = [];
        $this->_fileIsEmpty = false;
    }
}
