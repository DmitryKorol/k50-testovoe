<?php

/**
 * Class Board
 *
 * Класс реализующий доску с монетами.
 * Состояние доски, т.е. текущее положение монет на полях выражено в строке,
 * где каждый символ - отдельное поле со значениями по умолчанию:
 * 0 - поле пустое, 1 - поле занято монетой
 * Отсчёт полей слева направо, от 1 до размера доски.
 */
class Board
{
    const FIELD_FREE = '0';
    const FIELD_COIN = '1';

    /** @var integer Размер доски, т.е. колличество полей */
    private $_size;

    /** @var integer Колличество монет */
    private $_coinsCount;

    /** @var integer Необходимое колличество сдвигов для просчёта комбинаций */
    private $_requiredShiftsCount;

    /** @var string Базовое состояние доски после инициализации */
    private $_primaryState;

    /** @var string Текущее состояние доски */
    private $_currentState;

    /**
     * Board constructor.
     * @param int $size Размер доски
     * @param int $coinsCount Колличество монет
     */
    public function __construct(int $size, int $coinsCount)
    {
        if($size < 1 || $coinsCount < 1)
            throw new \InvalidArgumentException("Размер поля и колличество монет должны быть больше 0");

        if($size < $coinsCount || $size == $coinsCount)
            throw new \InvalidArgumentException("Колличество монет не может быть равным или превышать размера поля.");

        $this->_size = $size;
        $this->_coinsCount = $coinsCount;

        $this->_initBoard();
    }

    /**
     * Возвращает колличество комбинаций для текущего стола
     * @return int
     */
    public function getCombinationsCount() : int
    {
        $count = 0;
        $freeFieldsCount = $this->_size - $this->_coinsCount;

        $count += ($this->_size - $this->_coinsCount) * $this->_coinsCount + 1;
        for($offset = 1; $offset <= $this->_requiredShiftsCount; $offset++) {
            $count += ($this->_size - $offset - $this->_coinsCount) * $this->_coinsCount - ($freeFieldsCount - $offset);
        }

        return $count;
    }

    /**
     * Ищет комбинации, при нахождении каждой комбинации вызывает анонимную функцию из первого аргумента
     * @param Closure $newCombinationCallback
     */
    public function findCombinations(\Closure $newCombinationCallback) : void
    {
        for($currentOffset = 0; $currentOffset <= $this->_requiredShiftsCount; $currentOffset++) {
            $this->_currentState = $this->_primaryState;
            $this->_shiftCoinsRightFewTimes($currentOffset);
            $newCombinationCallback($this->_currentState);

            // Обходим каждую монету справа налево, с учётом смещения, если смещение ненулевое, то последнюю монету пропускаем
            $countCoinsUntil = $currentOffset == 0 ? $currentOffset : $currentOffset + 1;
            for($currentCoinPosition = $this->_coinsCount + $currentOffset; $currentCoinPosition > $countCoinsUntil; $currentCoinPosition--) {
                // Помещаем текущую монету на каждое свободное поле слева направо
                $lastFreeFieldNumber = $this->_size - ($this->_coinsCount - $currentCoinPosition) - 1 - $currentOffset;
                // Если текущее смещение больше одного и текущая монета предпоследняя, то её помещать на последнюю позицию не требуется
                if($currentOffset != 0 && $currentCoinPosition == $countCoinsUntil + 1)
                    $lastFreeFieldNumber--;

                for($newPosition = $currentCoinPosition; $newPosition <= $lastFreeFieldNumber; $newPosition++) {
                    if($this->_changeCoinPosition($newPosition, $newPosition + 1))
                        $newCombinationCallback($this->_currentState);
                }
            }
        }
    }

    /**
     * Инициализирует стол, а именно формирует начальное состояние исходя от размера и колличества монет
     */
    private function _initBoard() : void
    {
        $this->_primaryState = '';
        for($i = 1; $i <= $this->_size; $i++) {
            if($i <= $this->_coinsCount)
                $this->_primaryState .= self::FIELD_COIN;
            else
                $this->_primaryState .= self::FIELD_FREE;
        }

        $this->_requiredShiftsCount = $this->_coinsCount == 1 ? 0 : $this->_size - $this->_coinsCount - 1;
    }

    /**
     * Вовращает значение поля
     * @param int $number
     * @return string
     */
    private function _getField(int $number) : string
    {
        if($number > $this->_size)
            throw new OutOfRangeException();

        return $this->_currentState{$number - 1};
    }

    /**
     * Устанавливает полю значение
     * @param int $number
     * @param string $value
     */
    private function _setField(int $number, string $value) : void
    {
        if($number > $this->_size)
            throw new OutOfRangeException();

        if(strlen($value) > 1 || !in_array($value, [self::FIELD_FREE, self::FIELD_COIN]))
            throw new BadMethodCallException();

        $this->_currentState{$number - 1} = $value;
    }

    /**
     * Устанавливает значения нескольким полям
     * @param array $values
     */
    private function _setFields(array $values)
    {
        foreach($values as $fieldNumber => $fieldPosition)
            $this->_setField($fieldNumber, $fieldPosition);
    }

    /**
     * Проверяет пустое ли поле
     * @param int $number
     * @return bool
     */
    private function _isFieldFree(int $number) : bool
    {
        return $this->_getField($number) == self::FIELD_FREE;
    }

    /**
     * Переставляет монету с одного поля на другое
     * @param int $from
     * @param int $to
     * @return bool
     */
    private function _changeCoinPosition(int $from, int $to) : bool
    {
        if($this->_isFieldFree($from) || !$this->_isFieldFree($to))
            return false;

        $this->_setFields([
            $from => self::FIELD_FREE,
            $to => self::FIELD_COIN,
        ]);

        return true;
    }

    /**
     * Сдвигает монеты вправо на заданное колличество раз
     * @param int $timesCount
     */
    private function _shiftCoinsRightFewTimes(int $timesCount) : void
    {
        for($i = 1; $i <= $timesCount; $i++)
            $this->_shiftCoinsRight();
    }

    /**
     * Сдвигает монеты вправо на одну монету
     */
    private function _shiftCoinsRight() : void
    {
        $newState = '';
        for($i = 1; $i <= $this->_size; $i++) {
            if($i != $this->_size)
                $newState .= $this->_getField($i);
            else
                $newState = $this->_getField($i) . $newState;
        }

        $this->_currentState = $newState;
    }
}
